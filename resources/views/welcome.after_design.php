<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>MarksMen Fashion</title>
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" type="image/png">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/LineIcons.css') }}">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/default.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>

<body>
    <div class="preloader">
        <div class="loader">
            <div class="ytp-spinner">
                <div class="ytp-spinner-container">
                    <div class="ytp-spinner-rotator">
                        <div class="ytp-spinner-left">
                            <div class="ytp-spinner-circle"></div>
                        </div>
                        <div class="ytp-spinner-right">
                            <div class="ytp-spinner-circle"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="header-area">
        <div class="navbar-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <nav class="navbar navbar-expand-lg">
                            <a class="navbar-brand" href="#">
                                <img src="{{ asset('images/logo.png') }}" alt="Marksmen Bangladesh Logo">
                            </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarEight" aria-controls="navbarEight" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="toggler-icon"></span>
                                <span class="toggler-icon"></span>
                                <span class="toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse sub-menu-bar" id="navbarEight">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item active">
                                        <a class="page-scroll" href="#home">HOME</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="page-scroll" href="#about">ABOUT</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="page-scroll" href="#howWeWork">How We Work</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="page-scroll" href="#portfolio">PORTFOLIO</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="page-scroll" href="#testimonial">CLIENTS</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="page-scroll" href="#contact">CONTACT</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <div id="home" class="slider-area">
            <div class="bd-example">
                <div id="carouselOne" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselOne" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselOne" data-slide-to="1"></li>
                        <li data-target="#carouselOne" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item bg_cover active" style="background-image: url({{ asset('images/slider-1.jpg') }})">
                            <div class="carousel-caption">
                                <div class="container">
                                    <div class="row justify-content-center">
                                        <div class="col-xl-6 col-lg-7 col-sm-10">
                                            <h2 class="carousel-title">Refreshing Design & Easy to Customize</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item bg_cover" style="background-image: url({{ asset('images/slider-2.jpg') }})">
                            <div class="carousel-caption">
                                <div class="container">
                                    <div class="row justify-content-center">
                                        <div class="col-xl-6 col-lg-7 col-sm-10">
                                            <h2 class="carousel-title">Based on Latest Bootstrap & HTML5</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item bg_cover" style="background-image: url({{ asset('images/slider-3.jpg') }})">
                            <div class="carousel-caption">
                                <div class="container">
                                    <div class="row justify-content-center">
                                        <div class="col-xl-6 col-lg-7 col-sm-10">
                                            <h2 class="carousel-title">Multi-purpose Landing Page Template</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselOne" role="button" data-slide="prev">
                        <i class="lni-arrow-left-circle"></i>
                    </a>
                    <a class="carousel-control-next" href="#carouselOne" role="button" data-slide="next">
                        <i class="lni-arrow-right-circle"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <div class="overlay-right"></div>

    <section id="about" class="portfolio-area">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-5">
                    <h3 class="title text-center">About Us</h3>
                </div>
                <div class="col-lg-6" style="margin-top: 70px;">
                    <div class="about-image text-center wow fadeInUp" data-wow-duration="1.5s" data-wow-offset="100">
                        <img src="{{ asset('images/services.png') }}" alt="Marksmen Bangladesh">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="section-title pb-20">
                        <p class="text">Marksmen Fashion is an apparel manufacturer and exporter in Bangladesh. We manufacture and supply a wide range of readymade garments for knitwear, woven apparels, sweater attire, socks etc. We have an industry leading reputation for carrying good quality products at competitive prices with the reasonable and shortest lead-times</p>
                        <p class="text">We have allies of wide ranged internationally reputed manufacturers and own manufacturing units to meet the customers’ requirements on priority basis. We manufacture and supply a wide range of affordable warehouse based garments including standard and high fashion embellished attire.</p>
                        <p class="text">We strictly adhere to quality assurance procedures at every stage of production to ensure international quality and keep delivery schedule intact. We rely heavily on our ability and commitment to deliver better products and services, whether these products are for up-market brand stores or chained superstores or off-price channels or individual retailers we always maintain product excellence.</p>
                    </div>
                </div>
                <div class="col-12 ">
                    <div class="section-title pb-20">
                        <p class="text">Our objective is to achieve customer satisfaction by delivering quality and keeping promises and to maintain that trust through continuous improvement. We work on the basis of taking responsibility and partnership, not just working for the sake of work. Customer’s trust and confidence is our assets, we firmly believe that when our customers start working with us, they will find us a competent, dedicated and long-term business partner.</p>
                        <p class="text">Welcome to our arena, we are always happy to assist you. Sincerely awaiting to know your requirements.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="howWeWork" class="about-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="section-title text-center mt-30 pb-40">
                        <h4 class="title wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.6s">The future of designing starts here</h4>
                        <p class="text wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1s">An open platform for presentations and content collaboration. Sign up to get early access.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="single-about d-sm-flex mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1.2s">
                        <div class="about-icon">
                            <img src="{{ asset('images/icon-1.png') }}" alt="Marksmen Bangladesh">
                        </div>
                        <div class="about-content media-body">
                            <h4 class="about-title">Powerful templates</h4>
                            <p class="text">Poorly designed presentations are a thing of the past. Create beautiful and high-quality content that is aligned.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-about d-sm-flex mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1.4s">
                        <div class="about-icon">
                            <img src="{{ asset('images/icon-2.png') }}" alt="Marksmen Bangladesh">
                        </div>
                        <div class="about-content media-body">
                            <h4 class="about-title">Designed for everyone</h4>
                            <p class="text">Poorly designed presentations are a thing of the past. Create beautiful and high-quality content that is aligned.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-about d-sm-flex mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1.6s">
                        <div class="about-icon">
                            <img src="{{ asset('images/icon-3.png') }}" alt="Marksmen Bangladesh">
                        </div>
                        <div class="about-content media-body">
                            <h4 class="about-title">Work anywhere</h4>
                            <p class="text">Poorly designed presentations are a thing of the past. Create beautiful and high-quality content that is aligned.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-about d-sm-flex mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1.8s">
                        <div class="about-icon">
                            <img src="{{ asset('images/icon-4.png') }}" alt="Marksmen Bangladesh">
                        </div>
                        <div class="about-content media-body">
                            <h4 class="about-title">Updated in real time</h4>
                            <p class="text">Poorly designed presentations are a thing of the past. Create beautiful and high-quality content that is aligned.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="portfolio" class="portfolio-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center pb-20">
                        <h3 class="title">Our Portfolio</h3>
                        <p class="text">Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed!</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="portfolio-menu pt-30 text-center">
                        <ul>
                            <li data-filter="*" class="active">ALL WORK</li>
                            <li data-filter=".branding-3">BRANDING</li>
                            <li data-filter=".marketing-3">MARKETING</li>
                            <li data-filter=".planning-3">PLANNING</li>
                            <li data-filter=".research-3">RESEARCH</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row grid">
                <div class="col-lg-4 col-sm-6 branding-3 planning-3">
                    <div class="single-portfolio mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.2s">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio-1-f.png') }}" alt="Marksmen Bangladesh">
                            <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                <div class="portfolio-content">
                                    <div class="portfolio-icon">
                                        <a class="image-popup" href="{{ asset('images/portfolio-1.png') }}"><i class="lni-zoom-in"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-text">
                            <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                            <p class="text">Short description for the ones who look for something new. Awesome!</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 marketing-3 research-3">
                    <div class="single-portfolio mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.4s">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio-2-f.png') }}" alt="Marksmen Bangladesh">
                            <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                <div class="portfolio-content">
                                    <div class="portfolio-icon">
                                        <a class="image-popup" href="{{ asset('images/portfolio-2.png') }}"><i class="lni-zoom-in"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-text">
                            <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                            <p class="text">Short description for the ones who look for something new. Awesome!</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 branding-3 marketing-3">
                    <div class="single-portfolio mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.7s">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio-3-f.png') }}" alt="Marksmen Bangladesh">
                            <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                <div class="portfolio-content">
                                    <div class="portfolio-icon">
                                        <a class="image-popup" href="{{ asset('images/portfolio-3.png') }}"><i class="lni-zoom-in"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-text">
                            <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                            <p class="text">Short description for the ones who look for something new. Awesome!</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 planning-3 research-3">
                    <div class="single-portfolio mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.2s">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio-4-f.png') }}" alt="Marksmen Bangladesh">
                            <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                <div class="portfolio-content">
                                    <div class="portfolio-icon">
                                        <a class="image-popup" href="{{ asset('images/portfolio-4.png') }}"><i class="lni-zoom-in"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-text">
                            <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                            <p class="text">Short description for the ones who look for something new. Awesome!</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 marketing-3">
                    <div class="single-portfolio mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.4s">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio-5-f.png') }}" alt="Marksmen Bangladesh">
                            <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                <div class="portfolio-content">
                                    <div class="portfolio-icon">
                                        <a class="image-popup" href="{{ asset('images/portfolio-5.png') }}"><i class="lni-zoom-in"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-text">
                            <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                            <p class="text">Short description for the ones who look for something new. Awesome!</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 planning-3">
                    <div class="single-portfolio mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.7s">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio-6-f.png') }}" alt="Marksmen Bangladesh">
                            <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                <div class="portfolio-content">
                                    <div class="portfolio-icon">
                                        <a class="image-popup" href="{{ asset('images/portfolio-6.png') }}"><i class="lni-zoom-in"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-text">
                            <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                            <p class="text">Short description for the ones who look for something new. Awesome!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="call-action" class="call-action-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5">
                    <div class="call-action-content mt-45">
                        <h3 class="action-title">Get latest updates!</h3>
                        <p class="text">We never spam your email</p>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="call-action-form mt-50">
                        <form action="javascript:void(0)">
                            <input type="text" placeholder="Enter your email" required>
                            <div class="action-btn rounded-buttons">
                                <button class="main-btn rounded-three">subscribe</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="testimonial" class="testimonial-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center pb-20">
                        <h3 class="title">Testimonial</h3>
                        <p class="text">Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed!</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row testimonial-active">
                        <div class="col-lg-4">
                            <div class="single-testimonial mt-30 mb-30 text-center">
                                <div class="testimonial-image">
                                    <img src="{{ asset('images/author-3.jpg') }}" alt="Marksmen Bangladesh">
                                </div>
                                <div class="testimonial-content">
                                    <p class="text">Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed! Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed!</p>
                                    <h6 class="author-name">Isabela Moreira</h6>
                                    <span class="sub-title">CEO, GrayGrids</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="single-testimonial mt-30 mb-30 text-center">
                                <div class="testimonial-image">
                                    <img src="{{ asset('images/author-1.jpg') }}" alt="Marksmen Bangladesh">
                                </div>
                                <div class="testimonial-content">
                                    <p class="text">Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed! Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed!</p>
                                    <h6 class="author-name">Fiona</h6>
                                    <span class="sub-title">Lead Designer, UIdeck</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="single-testimonial mt-30 mb-30 text-center">
                                <div class="testimonial-image">
                                    <img src="{{ asset('images/author-2.jpg') }}" alt="Marksmen Bangladesh">
                                </div>
                                <div class="testimonial-content">
                                    <p class="text">Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed! Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed!</p>
                                    <h6 class="author-name">Elon Musk</h6>
                                    <span class="sub-title">CEO, SpaceX</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="single-testimonial mt-30 mb-30 text-center">
                                <div class="testimonial-image">
                                    <img src="{{ asset('images/author-4.jpg') }}" alt="Marksmen Bangladesh">
                                </div>
                                <div class="testimonial-content">
                                    <p class="text">Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed! Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed!</p>
                                    <h6 class="author-name">Fajar Siddiq</h6>
                                    <span class="sub-title">CEO, MakerFlix</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="client" class="client-logo-area">
        <div class="container">
            <div class="row client-active">
                <div class="col-lg-3">
                    <div class="single-client text-center">
                        <img src="{{ asset('images/client_logo_01.png') }}" alt="Marksmen Bangladesh">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single-client text-center">
                        <img src="{{ asset('images/client_logo_02.png') }}" alt="Marksmen Bangladesh">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single-client text-center">
                        <img src="{{ asset('images/client_logo_03.png') }}" alt="Marksmen Bangladesh">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single-client text-center">
                        <img src="{{ asset('images/client_logo_04.png') }}" alt="Marksmen Bangladesh">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single-client text-center">
                        <img src="{{ asset('images/client_logo_05.png') }}" alt="Marksmen Bangladesh">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single-client text-center">
                        <img src="{{ asset('images/client_logo_06.png') }}" alt="Marksmen Bangladesh">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single-client text-center">
                        <img src="{{ asset('images/client_logo_07.png') }}" alt="Marksmen Bangladesh">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="single-client text-center">
                        <img src="{{ asset('images/client_logo_08.png') }}" alt="Marksmen Bangladesh">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="contact" class="contact-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center pb-20">
                        <h3 class="title">Get in touch</h3>
                        <p class="text">Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed!</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="contact-two mt-50 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.2s">
                        <h4 class="contact-title">Lets talk about the project</h4>
                        <p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam unde repellendus delectus facilis quia consequatur maxime perferendis! Sequi, modi consequatur.</p>
                        <ul class="contact-info">
                            <li><i class="lni-money-location"></i> Dhanmondi, Dhaka, Bangladesh</li>
                            <li><i class="lni-phone-handset"></i> +01717 XXX XXX</li>
                            <li><i class="lni-envelope"></i> help@markmenbd.com</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="contact-form form-style-one mt-35 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.5s">
                        <form id="contact-form" action="javascript:void(0)" method="post">
                            <div class="form-input mt-15">
                                <label>Name</label>
                                <div class="input-items default">
                                    <input type="text" placeholder="Name" name="name" required>
                                    <i class="lni-user"></i>
                                </div>
                            </div>
                            <div class="form-input mt-15">
                                <label>Email</label>
                                <div class="input-items default">
                                    <input type="email" placeholder="Email" name="email" required>
                                    <i class="lni-envelope"></i>
                                </div>
                            </div>
                            <div class="form-input mt-15">
                                <label>Massage</label>
                                <div class="input-items default">
                                    <textarea placeholder="Massage" name="massage" required></textarea>
                                    <i class="lni-pencil-alt"></i>
                                </div>
                            </div>
                            <p class="form-message"></p>
                            <div class="form-input rounded-buttons mt-20">
                                <button type="submit" class="main-btn rounded-three">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer id="footer" class="footer-area">
        <div class="footer-copyright">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <div class="copyright text-center text-lg-left mt-10">
                            <p class="text">Developed by <a href="mailto:rejohnbd@gmail.com">Rejohn</a></p>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="footer-logo text-center mt-10">
                            <a href="index.html"><img src="{{ asset('images/favicon.png') }}" alt="Logo"></a>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <ul class="social text-center text-lg-right mt-10">
                            <li><a href="#"><i class="lni-facebook-filled"></i></a></li>
                            <li><a href="#"><i class="lni-twitter-original"></i></a></li>
                            <li><a href="#"><i class="lni-instagram-original"></i></a></li>
                            <li><a href="#"><i class="lni-linkedin-original"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <a href="#" class="back-to-top"><i class="lni-chevron-up"></i></a>

    <script src="{{ asset('vendor/modernizr-3.6.0.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/slick.min.js') }}"></script>
    <script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('js/scrolling-nav.js') }}"></script>
    <script src="{{ asset('js/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
</body>

</html>